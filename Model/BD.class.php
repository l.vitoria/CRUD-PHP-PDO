<?php

class BD {

    private $host = "localhost";
    private $usuario = "postgres";
    private $senha = "123";
    private $banco = "loja";
    private $porta = "5432";

  
    public function __construct() {

     try {
                 $this->pdo = new PDO("pgsql:host={$this->host}; dbname={$this->banco};  user={$this->usuario};  password={$this->senha}");
                 $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
     
            } catch (PDOException  $e) {
                 print $e->getMessage();
            }
        }
    
 }
